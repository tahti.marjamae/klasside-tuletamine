﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klasside_tuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom kroko = new Loom("Krokodill");
            Loom ämmelgas = new Loom("ämblik");
            Loom elajas = new Loom();
            Kass k = new Kass("Miisu", "angoora");

            //kroko.TeeHäält();            
            //ämmelgas.TeeHäält();        
            //elajas.TeeHäält();               
            //k.TeeHäält();

            List<Loom> loomad = new List<Loom>
            {
                kroko, ämmelgas, elajas, k, new Koduloom("lammas", "Dolly")
            };
            //Loom.Söö(ämmelgas);
            //kroko.SööÄra(ämmelgas);
            
            foreach (var x in loomad) x.TeeHäält();
            foreach (var x in loomad) Console.WriteLine(x);
            foreach (var x in loomad) x.SikutaSabast();


            k.Silita();
            k.TeeHäält();
            k.SikutaSabast();
            k.TeeHäält();
            
            Koer koer = new Koer("Polla");
            Console.WriteLine(koer);
            koer.TeeHäält();

            //TODO: Lõuna(k)
        }

            //public static void Lõuna(object x)
            //{
            //    //if (x is ISöödav) ((ISöödav)x).Süüakse();
            //    //if (x is ISöödav xs) xs.Süüakse();                // is - true-false
            //    (x as ISöödav)?.Süüakse();                            // as (tyyp)objekt või null
            //}
    }

    class Loom
    {
        public string Liik;
        public Loom(string liik) => Liik = liik;    //konstruktor
        //default konstruktor
        public Loom() : this("tundmatu liik") { } //this tähendab, et ta kutsub välja teise konstruktori ja annab talle tema parameetri ette        
        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält"); //virtual - tuletatud klassid võib meetodi(funktsiooni) üle kirjutada
        //public void SööÄra(Loom l) => Console.WriteLine($"{l.Liik} pistetakse nahka");
        //public static void Söö(Loom l) => Console.WriteLine($"{l.Liik} pistetakse nahka");

        public override string ToString() => $"loom liigist {Liik}";
        
        public void SikutaSabast() => Console.WriteLine($"Hoiatus! Looma {Liik} sabast sikutamine ei ole hea!"); //kui ei ole virtual, siis kassi juures ei kirjutata
    }
   
    class Koduloom : Loom  //koduloomal on vaikimisi kaasas konstruktor. Kõigepealt pean tegema looma ja siis kodulooma (vt üleval kommentaari)
    {
        public string Nimi;
        public Koduloom(string liik, string nimi) : base(liik) => Nimi = nimi;
        //lokaalne ehk klassi sees olev nimi on väikese tähega ning klassinimi on suure tähega. saaks asendada väikese tähega "this.Nimi"
        public override void TeeHäält() => Console.WriteLine($"{Nimi} möriseb mõnusasti"); //metsloomad teevad koledat häält aga koduloomad mörisevad mõnusasti
        public override string ToString()
        {
            return "kodu"+base.ToString();
        }
    }
    class Kass : Koduloom
    {
        public string Tõug;
        public bool Tuju = false;
        //konstruktor
        public Kass(string nimi, string tõug) : base("Kass", nimi) => tõug = Tõug;
        public void Silita() => Tuju = true;
        public new void SikutaSabast() => Tuju = false; //kuna üleval on sama nimega funktsioon, siis "new" teeb uue funktsiooni
        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }
    }

    abstract class Ahaa //siin võivad olla abstraktsed meetodid, aga ka pärismeetodid
    {
        public abstract void Abstr();
        public virtual void Tore() => Console.WriteLine("on ju tore");
    }
    class Ehee : Ahaa
    {
        public override void Abstr()
        {
            throw new NotImplementedException();
        }      
    }
    interface ISöödav       //siin on ainult abstraktsed asjad
    {
        void Süüakse();
    }
    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi sööb sepikut");
        }
    }
    class Koer : Koduloom, ISöödav
    {
        public string Tõug = "krants";
        public Koer(string nimi) : base("Koer", nimi) { }
        public override string ToString() => $"Koer {Nimi}";
        public override void TeeHäält() => Console.WriteLine($"KOer {Nimi} haugub");

        public void Süüakse()
        {
            Console.WriteLine($"Koer {Nimi} pistetakse nahka");
        }
    }
}
