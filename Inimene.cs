﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klasside_tuletamine
{
    class Inimene
    {
        static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed = _Inimesed.Values;
        string IK { get; }
        public string Nimi { get; set; }
        private Inimene(string ik)
        {
            IK = ik;
            _Inimesed.Add(ik, this);
        }
        static public Inimene Get(string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;
        static public Inimene New(string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : new Inimene(ik);
        public override string ToString() => $"{Nimi} IK:{IK}";


    }
}
